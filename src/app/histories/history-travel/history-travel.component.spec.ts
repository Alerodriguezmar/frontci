import { ComponentFixture, TestBed  } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { of } from 'rxjs';
import { HistoryTravelComponent } from "./history-travel.component";
import { HistoryTravelService } from "./history-travel.service";
import { IHistory } from "./ihistory";

describe('History component', () => {
  let travelHttpSpy: jasmine.SpyObj<HistoryTravelService>;
  let fixture: ComponentFixture<HistoryTravelComponent>
  let component: HistoryTravelComponent;
  

  beforeEach((async() => {

    travelHttpSpy = jasmine.createSpyObj<HistoryTravelService>('HistoryTravelService',['getAllHistory']);  
    
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        { provide: HistoryTravelService, useValue:travelHttpSpy }
      ],
      declarations: [HistoryTravelComponent]
    }).compileComponents()
  }))
  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    HistoryTravelComponent.prototype.ngOnInit = () => {} ;
    fixture = TestBed.createComponent(HistoryTravelComponent);
    component = fixture.componentInstance
    fixture.detectChanges();
  })

  it('instance components', () => {
    expect(component).toBeDefined();
    expect(component).toBeInstanceOf(HistoryTravelComponent);
    expect(component.histories.length).toBe(0);
  })

  it('load History', () => {
    travelHttpSpy.getAllHistory.and.returnValue(of( [
      {
        idHistory: '1',
        cityName: 'Funza',
        touristName: 'Diego Rodriguez',
        touristIdentification: '1073252824',
        admissionDate:  new Date(2021,11,17)} as IHistory,
        {
          idHistory: '2',
          cityName: 'Mosquera',
          touristName: 'Camilo Martinez',
          touristIdentification: '10252632',
          admissionDate:  new Date(2021,10,17)}as IHistory
  ]));
    component.loadHistory();
    expect(component.histories.length).toBe(2);
  })
})