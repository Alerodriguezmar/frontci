import { of } from "rxjs";
import { HistoryTravelService } from "./history-travel.service";
import { IHistory } from "./ihistory";

describe('History service', () => {

  let httpClientSpy: { get: jasmine.Spy }
  let travelService: HistoryTravelService;
  let expectHistory: IHistory[];
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    travelService = new HistoryTravelService(httpClientSpy as never);


    expectHistory = [
      {
        idHistory: '1',
        cityName: 'Funza',
        touristName: 'Diego Rodriguez',
        touristIdentification: '1073252824',
        admissionDate:  new Date(2021,11,17)},
        {
          idHistory: '2',
          cityName: 'Mosquera',
          touristName: 'Camilo Martinez',
          touristIdentification: '10252632',
          admissionDate:  new Date(2021,10,17)}
  ];
  });

  it('should return expected cities(HttpClient called)', () => {
    httpClientSpy.get.and.returnValue(of(expectHistory));
    travelService.getAllHistory().subscribe(
      histories => {
        expect(histories).toEqual(expectHistory)
          expect(histories.length).toEqual(2);
      });
  });
});
