export interface IHistory {
  idHistory: string;
  cityName: string;
  touristName: string;
  touristIdentification: string;
  admissionDate: Date;
}
