import { Component, OnInit } from '@angular/core'
import { HistoryTravelService } from './history-travel.service'
import { IHistory } from './ihistory';

@Component({
  selector: 'app-history-travel',
  templateUrl: './history-travel.component.html',
  styleUrls: ['./history-travel.component.css']
})
export class HistoryTravelComponent implements OnInit {
titulo = 'Historial Viajes';
  histories: IHistory[] = []; 

constructor (private historyTravelService: HistoryTravelService) { }

ngOnInit (): void {
  this.loadHistory();
}

loadHistory():void{
  this.historyTravelService.getAllHistory().subscribe(
    history => { this.histories = history }
  )
}
}
