import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { environment } from 'src/environments/environment'
import { IHistory } from './ihistory';

@Injectable({
  providedIn: 'root'
})
export class HistoryTravelService {
private url: string = environment.baseUrl + '/historial';

constructor (private Http: HttpClient) { }

/**
    * obtiene todos los registros de tipo historial.
    * @param url url a la cual se le enviapara la peticion http get.
    * @returns retorna una lista de todos los registros de viajes asignados.
    */
getAllHistory (): Observable<IHistory[]> {
  return this.Http.get<IHistory[]>(this.url)
}
}
