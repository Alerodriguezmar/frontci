import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoriesRoutingModule } from './histories-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HistoryTravelComponent } from './history-travel/history-travel.component';


@NgModule({
  declarations: [HistoryTravelComponent],
  imports: [
    CommonModule,
    HistoriesRoutingModule,
    HttpClientModule,
    FormsModule
  ]
})
export class HistoriesModule { }
