import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistoryTravelComponent } from './history-travel/history-travel.component';

const routes: Routes = [
  {
    path:'',
    component:HistoryTravelComponent

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoriesRoutingModule { }
