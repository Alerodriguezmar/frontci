/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OpenweatherService {

  constructor(private Http: HttpClient) { }

  getWeather(cityName:string){
    return this.Http.get(environment.UrlWeather+cityName+'&units=metric&appid='+environment.keyWeather)
  }
}
