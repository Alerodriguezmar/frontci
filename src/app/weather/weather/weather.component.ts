import { Component} from '@angular/core';
import { OpenweatherService } from './openweather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent {

  weatherTem;
  title='Reporte del clima'
  constructor(private weatherservice:OpenweatherService ) { }

  getweathercity(cityName:string):void{
     this.weatherservice.getWeather(cityName).subscribe(
         err => {this.weatherTem = err
           console.log(err);
        }   
      )
  }
}
