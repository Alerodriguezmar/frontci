import { ComponentFixture, TestBed} from "@angular/core/testing";
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ITourist } from "./itourist";
import { TouristComponent } from "./tourist.component";
import { TouristService } from "./tourist.service";

describe('Tourist Component', () => {
  let touristHttpSpy: jasmine.SpyObj<TouristService>;
  let fixture: ComponentFixture<TouristComponent>
  let component: TouristComponent;
  

  beforeEach((async () => {

    touristHttpSpy = jasmine.createSpyObj<TouristService>('TouristService',['getAllTourist','deleteTourist','createTourist']);  
    
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        { provide: TouristService, useValue:touristHttpSpy }
      ],
      declarations: [TouristComponent]
    }).compileComponents()
  }))
  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    TouristComponent.prototype.ngOnInit = () => {} ;
    fixture = TestBed.createComponent(TouristComponent);
    component = fixture.componentInstance
    fixture.detectChanges();
  })
  it('instance components', () => {
    
    expect(component).toBeDefined();
    expect(component).toBeInstanceOf(TouristComponent);
    expect(component.tourists.length).toBe(0);
  })

  it('add Tourist', () => {
    component.tourists = []
    const tourist:ITourist = {
      idTourist: 1,
      touristName: 'Diego' ,
      touristLastname: 'Rodriguez',
      birthDate: new Date(2021,10,17),
      identification: '1073252824',
      typeId: 'CC',
      travelFrequency: 5,
      travelBudget: 10000,
      creditCard: true
    } as ITourist 

    expect(component.tourists.length).toBe(0);
    touristHttpSpy.createTourist.and.returnValue(of(tourist))
    component.create(tourist);
    expect(component.tourists.length).toBe(1);
    expect(component.tourists[0]).toBe(tourist)
  })
  it('load Tourists', () => {
    
    touristHttpSpy.getAllTourist.and.returnValue(of( [
      {
        idTourist: 1,
        touristName: 'Diego' ,
        touristLastname: 'Rodriguez',
        birthDate: new Date(2021,10,17),
        identification: '1073252824',
        typeId: 'CC',
        travelFrequency: 5,
        travelBudget: 10000,
        creditCard: true
      } as ITourist
      
    ]));
    component.loadTourist();
    expect(component.tourists.length).toBe(1);
  })

  it('remove tourist', () => {
   
    component.tourists = [   {
      idTourist: 1,
      touristName: 'Diego' ,
      touristLastname: 'Rodriguez',
      birthDate: new Date(2021,10,17),
      identification: '1073252824',
      typeId: 'CC',
      travelFrequency: 5,
      travelBudget: 10000,
      creditCard: true
    } as ITourist ]
    expect(component.tourists.length).toBe(1);
   touristHttpSpy.deleteTourist.and.returnValue(of(component.tourists[0]))
    component.delete(component.tourists[0])
    expect(component.tourists.length).toBe(0);
    expect(component.delete).toBeTruthy();
  })

  it('add Tourist', () => {
    component.tourists = []
    const tourist:ITourist = {
      idTourist: 1,
      touristName: 'Diego' ,
      touristLastname: 'Rodriguez',
      birthDate: new Date(2021,10,17),
      identification: '1073252824',
      typeId: 'CC',
      travelFrequency: 5,
      travelBudget: 10000,
      creditCard: true
    } as ITourist 

    expect(component.tourists.length).toBe(0);
    touristHttpSpy.createTourist.and.returnValue(of(tourist))
    component.create(tourist);
    expect(component.tourists.length).toBe(1);
  })
})
