import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from 'src/environments/environment'
import { ITourist } from './itourist';

@Injectable({
  providedIn: 'root'
})
export class TouristService {
  private urlList: string = environment.baseUrl + '/turistas';
  private url: string = environment.baseUrl + '/turista';

  constructor (private Http: HttpClient) { }

  /**
     * Obtiene todos los turistas registrados
     * @param urlList url a la cual se enviara la peticion get http para obtener todos los turistas registrados.
     * @returns retorna todos los turistas registrados..
     */
  getAllTourist (): Observable<ITourist[]> {
    return this.Http.get<ITourist[]>(this.urlList)
  }

  /**
   * Envia un objeto de tipo tourist para su creacion o insercion
   * @param url url a la cual enviara la peticion post http para la creacion del objeto tipo tourist.
   * @param tourist objeto de tipo ciudad el cual va a ser enviado
   */
  createTourist (tourist: ITourist): Observable<ITourist> {
    return this.Http.post<ITourist>(this.url, tourist)
  }

  /**
   * Obitiene un objeto unico de tipo tourist agregando su identificador a la url
   * @param idTourist identificador de la ciudad a buscar o retornar.
   * @param url url a la cual se enviara la peticion http get y se le añadira el identificador
   * @returns un turista con un identificador de valor idTourist.
   */
  getTourist (idTourist: number): Observable<ITourist> {
    return this.Http.get<ITourist>(this.url + '/' + idTourist)
  }

  /**
   * Actualiza un objeto de tipo tourist ya existente.
   * @param url url a la cual se enviara la peticion put http para actualizar un objeto tipo tourist.
   * @param tourist Nuevos valores a tomar un objeto tipo tourist ya registrado.
   */
  updateTourist (tourist: ITourist): Observable<ITourist> {
    return this.Http.put<ITourist>(this.url, tourist)
  }

  /**
   * Elimina un turista ya existente por su identificador.
   * @param url url a la cual se enviara la peticion http delete  y se le añadira el identificador.
   * @param id identificador del objeto tipo tourist a eliminar.
   */
  deleteTourist (id: number): Observable<ITourist> {
    return this.Http.delete<ITourist>(this.url + '/' + id)
  }
}
