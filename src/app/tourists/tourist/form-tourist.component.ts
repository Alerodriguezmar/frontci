import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ITourist } from './itourist';
import { TouristService } from './tourist.service'

@Component({
  selector: 'app-form-tourist',
  templateUrl: './form-tourist.component.html',
  styleUrls: ['./form-tourist.component.css']
})
export class FormTouristComponent implements OnInit {
  touristForm = {} as ITourist;
   
  titleForm = 'Registro Turista'

  constructor(private touristService: TouristService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargar()
  }

  create(): void {
    this.touristService.createTourist(this.touristForm).subscribe(
      () => this.router.navigate(['/turista'])
    )
  }

  cargar(): void {
    this.activatedRoute.params.subscribe(
      e => {
        const idTourist = e.idTourist
        if (idTourist) {
          this.touristService.getTourist(idTourist).subscribe(
            es => { this.touristForm = es }
          )
        }
      }
    )
  }

  update(): void {
    this.touristService.updateTourist(this.touristForm).subscribe(
      () => this.router.navigate(['/turista'])
    )
  }
}
