import { of } from "rxjs";
import { ITourist } from "./itourist";
import { TouristService } from "./tourist.service";


describe('Tourist Service', () => {
  let httpClientSpyC: { post: jasmine.Spy }
  let cityServiceC: TouristService;
  let httpClientSpy: { get: jasmine.Spy }
  let cityService: TouristService;
  let httpClientSpyD: { delete: jasmine.Spy }
  let cityServiceD: TouristService;
  let httpClientSpyUpdate: { put: jasmine.Spy }
  let cityServiceUpdate: TouristService;
  let expectcity: ITourist[];

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('TouristService', ['get']);
    cityService = new TouristService(httpClientSpy as never);
    httpClientSpyC = jasmine.createSpyObj('HttpClient', ['post']);
    cityServiceC = new TouristService(httpClientSpyC as never);
    httpClientSpyD = jasmine.createSpyObj('HttpClient', ['delete']);
    cityServiceD = new TouristService(httpClientSpyD as never);
    httpClientSpyUpdate = jasmine.createSpyObj('HttpClient', ['put']);
    cityServiceUpdate = new TouristService(httpClientSpyUpdate as never);

    expectcity =  [   {
      idTourist: 1,
      touristName: 'Diego' ,
      touristLastname: 'Rodriguez',
      birthDate: new Date(2021,10,17),
      identification: '1073252824',
      typeId: 'CC',
      travelFrequency: 5,
      travelBudget: 10000,
      creditCard: true
    } as ITourist ];

  });

  it('should return expected tourist(HttpClient called)', () => {


    httpClientSpy.get.and.returnValue(of(expectcity));

    cityService.getAllTourist().subscribe(
      cities => {
        expect(cities).toEqual(expectcity)
          expect(cities.length).toEqual(1);
      });
  });

  it('should create expected tourist(HttpClient called)', () => {
    const expectcity2: ITourist =
    {
      idTourist: 1,
      touristName: 'Diego' ,
      touristLastname: 'Rodriguez',
      birthDate: new Date(2021,10,17),
      identification: '1073252824',
      typeId: 'CC',
      travelFrequency: 5,
      travelBudget: 10000,
      creditCard: true
    } as ITourist 

    httpClientSpyC.post.and.returnValue(of(expectcity2))

    cityServiceC.createTourist(expectcity2).subscribe(
      cities => {
        expect(cities).toBe(expectcity2)
        expect(cityService.createTourist).toBeTruthy();
      });
  });


  it('should delete expected tourist(HttpClient called)', () => {
    const city = expectcity
    httpClientSpyD.delete.and.returnValue(of(true))
    cityServiceD.deleteTourist(city[0].idTourist).subscribe(
      cities => {
        expect(cities).toBeTrue
        expect(cityServiceD.deleteTourist).toBeTruthy()
      });
  });

  it('update', () => {
    const updateCity: ITourist =
    {
      idTourist: 1,
      touristName: 'Diego' ,
      touristLastname: 'Rodriguez',
      birthDate: new Date(2021,10,17),
      identification: '1073252824',
      typeId: 'CC',
      travelFrequency: 5,
      travelBudget: 10000,
      creditCard: true
    } as ITourist 
   
    httpClientSpyUpdate.put.and.returnValue(of(updateCity))
    
    cityServiceUpdate.updateTourist(updateCity).subscribe(
      cities => {
        expect(cities).toEqual(updateCity)  
        expect(cityServiceUpdate.updateTourist).toBeTruthy();      
      });
  });
});