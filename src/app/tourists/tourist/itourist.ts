export interface ITourist {
  idTourist: number;
  touristName: string ;
  touristLastname: string;
  birthDate: Date;
  identification: string;
  typeId: string;
  travelFrequency: number;
  travelBudget: number;
  creditCard: boolean;
}
