import { Component, OnInit } from '@angular/core'
import { ITourist } from './itourist';
import { TouristService } from './tourist.service'

@Component({
  selector: 'app-tourist',
  templateUrl: './tourist.component.html'
})
export class TouristComponent implements OnInit {
  titulo = 'Turistas';
  tourists: ITourist[] = []
  constructor(private touristService: TouristService) { }

  loadTourist(): void {
    this.touristService.getAllTourist().subscribe(
      tourist => { this.tourists = tourist }
    )
  }

  delete(tourist: ITourist): void {
    this.touristService.deleteTourist(tourist.idTourist).subscribe();
    this.tourists = this.tourists.filter(c => c.idTourist != tourist.idTourist)
  }

  create(tourist: ITourist): void {
    this.touristService.createTourist(tourist).subscribe(() => this.tourists.push(tourist))
  }

  ngOnInit(): void {
     this.loadTourist()
  }




}
