import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TouristsRoutingModule } from './tourists-routing.module';
import { FormTouristComponent } from './tourist/form-tourist.component';
import { TouristComponent } from './tourist/tourist.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormTravelComponent } from '../travels/travel/form-travel.component';


@NgModule({
  declarations: [
    TouristComponent,
    FormTouristComponent,
    FormTravelComponent
  ],
  imports: [
    CommonModule,
    TouristsRoutingModule,
    HttpClientModule,
    FormsModule
  ]
})
export class TouristsModule { }
