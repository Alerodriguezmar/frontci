import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormTravelComponent } from '../travels/travel/form-travel.component';
import { FormTouristComponent } from './tourist/form-tourist.component';
import { TouristComponent } from './tourist/tourist.component';

const routes: Routes = [

    {path:'',component:TouristComponent},
    { path: 'form', component: FormTouristComponent },
    { path: 'form/:idTourist', component: FormTouristComponent },
    { path: 'turista/formviaje/:idTourist', component: FormTravelComponent }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TouristsRoutingModule { }
