import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TravelsRoutingModule } from './travels-routing.module';
import { TravelComponent } from './travel/travel.component';

@NgModule({
  declarations: [
    TravelComponent
  ],
  imports: [
    CommonModule,
    TravelsRoutingModule
  ]
})
export class TravelsModule { }
