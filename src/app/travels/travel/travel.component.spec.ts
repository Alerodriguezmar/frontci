import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ITravel } from "./itravel";
import { TravelComponent } from "./travel.component";
import { TravelService } from "./travel.service";

describe('travel Component', () => {
  let travelHttpSpy: jasmine.SpyObj<TravelService>;
  let fixture: ComponentFixture<TravelComponent>
  let component: TravelComponent;

  beforeEach((async () => {

    travelHttpSpy = jasmine.createSpyObj<TravelService>('TravelService', ['getAllTravel', 'deleteTravel', 'createTravel']);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        { provide: TravelService, useValue: travelHttpSpy }
      ],
      declarations: [TravelComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    TravelComponent.prototype.ngOnInit = () => {} ;
    fixture = TestBed.createComponent(TravelComponent);
    component = fixture.componentInstance
    fixture.detectChanges();
  })

  it('instance component', async () => {

    expect(component).toBeDefined();
    expect(component).toBeInstanceOf(TravelComponent);
    expect(component.travels.length).toBe(0);
  })

  it('load travel', async () => {

    travelHttpSpy.getAllTravel.and.returnValue(of([{
      idTravel: 1,
      travelDate: new Date(2021, 10, 17),
      tourist: {
        idTourist: 1,
        touristName: 'Diego',
        touristLastname: 'Rodriguez',
        birthDate: new Date(2021, 10, 17),
        identification: '1073252824',
        typeId: 'CC',
        travelFrequency: 5,
        travelBudget: 10000,
        creditCard: true
      },
      city: {
        idCity: 1,
        cityName: 'Funza',
        amountPopulation: 59692,
        touristicPlace: 'Centro',
        reservedHotel: 'Principal'
      }
    }

    ]));
    component.loadTravel();
    expect(component.travels.length).toBe(1);
  })

  it('reomove travel', async () => {

    component.travels = [{
      idTravel: 1,
      travelDate: new Date(2021, 10, 17),
      tourist: {
        idTourist: 1,
        touristName: 'Diego',
        touristLastname: 'Rodriguez',
        birthDate: new Date(2021, 10, 17),
        identification: '1073252824',
        typeId: 'CC',
        travelFrequency: 5,
        travelBudget: 10000,
        creditCard: true
      },
      city: {
        idCity: 1,
        cityName: 'Funza',
        amountPopulation: 59692,
        touristicPlace: 'Centro',
        reservedHotel: 'Principal'
      }
    }]

    expect(component.travels.length).toBe(1);
    travelHttpSpy.deleteTravel.and.returnValue(of(component.travels[0]))
    component.delete(component.travels[0])
    expect(component.travels.length).toBe(0);
    expect(component.delete).toBeTruthy();
  })

  it('add Travel', () => {
    component.travels = []
    const travel: ITravel = {
      idTravel: 1,
      travelDate: new Date(2021, 10, 17),
      tourist: {
        idTourist: 1,
        touristName: 'Diego',
        touristLastname: 'Rodriguez',
        birthDate: new Date(2021, 10, 17),
        identification: '1073252824',
        typeId: 'CC',
        travelFrequency: 5,
        travelBudget: 10000,
        creditCard: true
      },
      city: {
        idCity: 1,
        cityName: 'Funza',
        amountPopulation: 59692,
        touristicPlace: 'Centro',
        reservedHotel: 'Principal'
      }
    } as ITravel

    expect(component.travels.length).toBe(0);
    travelHttpSpy.createTravel.and.returnValue(of(travel))
    //component.create(travel);
    //expect(component.travels.length).toBe(1);
  })
})