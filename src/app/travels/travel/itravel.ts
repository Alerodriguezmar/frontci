import { ICity } from "../../cities/city/icity";
import { ITourist } from "../../tourists/tourist/itourist";

export interface ITravel {
  idTravel: number;
  travelDate: Date;
  tourist: ITourist;
  city: ICity;
}
