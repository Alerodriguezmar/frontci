import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { environment } from 'src/environments/environment'
import { ITravel } from './itravel';

@Injectable({
  providedIn: 'root'
})
export class TravelService {
  private url: string = environment.baseUrl + '/viajes';

  constructor (private Http: HttpClient) { }

  /**
   * Obtiene todos los viajes registrados.
   * @param url url a la cual se enviara la peticion get http para obtener todos los viajes asignados.
   * @returns retorna todos los registros de asignacion de viajes.
   */
  getAllTravel (): Observable<ITravel[]> {
    return this.Http.get<ITravel[]>(this.url)
  }

  /**
    * Elimina un viaje ya existente por su identificador.
    * @param url url a la cual se enviara la peticion http delete  y se le añadira el identificador.
    * @param id identificador del objeto tipo travel a eliminar.
    */
  deleteTravel (id: number): Observable<ITravel> {
    return this.Http.delete<ITravel>(this.url + '/' + id)
  }

  /**
   * Envia un objeto de tipo travel para su creacion o insercion
   * @param url url a la cual enviara la peticion post http para la creacion del objeto.
   * @param travel objeto de tipo travel el cual va a ser enviado
   */
  createTravel (travel: ITravel , mail?:string): Observable<ITravel> {
    return this.Http.post<ITravel>(this.url + '/' + mail, travel)
  }
}
