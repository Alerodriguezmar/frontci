import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { CityService } from '../../cities/city/city.service'
import { ICity } from '../../cities/city/icity'
import { ITourist } from '../../tourists/tourist/itourist'
import { TouristService } from '../../tourists/tourist/tourist.service'
import { ITravel } from './itravel'
import { TravelService } from './travel.service'

@Component({
  selector: 'app-form-travel',
  templateUrl: './form-travel.component.html',
  styleUrls: ['./form-travel.component.css']
})
export class FormTravelComponent implements OnInit {
  cities: ICity[] | undefined;
  tourists: ITourist[] | undefined;
  travelForm = {} as ITravel;
  title='Asignacion Viaje'
  constructor (private cityService: CityService, private touristService: TouristService, private travelService: TravelService,
    private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit (): void {
    this.cargar()
    this.touristService.getAllTourist().subscribe(
      tourist => { this.tourists = tourist }
    )
    this.cityService.getAllCity().subscribe(
      city => { this.cities = city }
    )
  }

  create (mail:string): void {
    this.travelService.createTravel(this.travelForm,mail).subscribe(
      () => this.router.navigate(['/turista'])
    )
  }

  cargar (): void {
    this.activatedRoute.params.subscribe(
      e => {
        const idTourist = e.idTourist
        if (idTourist) {
          this.touristService.getTourist(idTourist).subscribe(
            es => { this.travelForm.tourist = es }
          )
        }
      }
    )
  }
}
