import { Component, OnInit } from '@angular/core'
import { ITravel } from './itravel';
import { TravelService } from './travel.service'

@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html'
})

export class TravelComponent implements OnInit {
  titulo = 'Viajes';
  travels: ITravel[] = [];
  constructor (private travelService: TravelService) { }

  ngOnInit (): void {
   this.loadTravel()
  }

  loadTravel():void{
    this.travelService.getAllTravel().subscribe(
      travel => { this.travels = travel }
    )
  }

  delete (travel: ITravel): void {
    this.travelService.deleteTravel(travel.idTravel).subscribe();
    this.travels = this.travels.filter(c => c.idTravel !=  travel.idTravel)   
}

create (travel: ITravel , mail:string): void {
  this.travelService.createTravel(travel,mail).subscribe(() => this.travels.push(travel))
}

}
