import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  
  {
    path: 'ciudad',
    loadChildren: () => import('./cities/cities.module').then(m => m.CitiesModule)
  },
  {
    path: 'turista',
    loadChildren: () => import('./tourists/tourists.module').then(m => m.TouristsModule)
  },
  {
    path: 'viajes',
    loadChildren: () => import('./travels/travels.module').then(m => m.TravelsModule)
  },
  {
    path: 'historial',
    loadChildren: () => import('./histories/histories.module').then(m => m.HistoriesModule)
  },
  {
    path: 'pronostico',
    loadChildren: () => import('./weather/weather.module').then(m => m.WeatherModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
