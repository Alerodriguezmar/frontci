import { of } from "rxjs";
import { CityService } from "./city.service"
import { ICity } from "./icity";

describe('service : city', () => {

  let httpClientSpyC: { post: jasmine.Spy }
  let cityServiceC: CityService;
  let httpClientSpy: { get: jasmine.Spy }
  let cityService: CityService;
  let httpClientSpyD: { delete: jasmine.Spy }
  let cityServiceD: CityService;
  let httpClientSpyUpdate: { put: jasmine.Spy }
  let cityServiceUpdate: CityService;
  let expectcity: ICity[];

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    cityService = new CityService(httpClientSpy as never);
    httpClientSpyC = jasmine.createSpyObj('HttpClient', ['post']);
    cityServiceC = new CityService(httpClientSpyC as never);
    httpClientSpyD = jasmine.createSpyObj('HttpClient', ['delete']);
    cityServiceD = new CityService(httpClientSpyD as never);
    httpClientSpyUpdate = jasmine.createSpyObj('HttpClient', ['put']);
    cityServiceUpdate = new CityService(httpClientSpyUpdate as never);

    expectcity = [
      {
        idCity: 1,
        cityName: 'Funza',
        amountPopulation: 59692,
        touristicPlace: 'Centro',
        reservedHotel: 'Principal'
      },
      {
        idCity: 2,
        cityName: 'Mosquera',
        amountPopulation: 22625,
        touristicPlace: 'Parque',
        reservedHotel: 'Luxor'
      }
    ];

  });

  it('should return expected cities(HttpClient called)', () => {

    httpClientSpy.get.and.returnValue(of(expectcity));

    cityService.getAllCity().subscribe(
      cities => {
        expect(cities).toEqual(expectcity)
          expect(cities.length).toEqual(2);
      });
  });

  it('should update expected city(HttpClient called)', () => {
    const expectcity2: ICity =
    {
      idCity: 1,
      cityName: 'Funza',
      amountPopulation: 59692,
      touristicPlace: 'Centro',
      reservedHotel: 'Principal'
    }

    httpClientSpyC.post.and.returnValue(of(expectcity2))

    cityServiceC.createCity(expectcity2).subscribe(
      cities => {
        expect(cities).toBe(expectcity2)
        expect(cityService.createCity).toBeTruthy();
      });
  });


  it('should delete expected city(HttpClient called)', () => {
    const city = expectcity
    httpClientSpyD.delete.and.returnValue(of(true))
    cityServiceD.deleteCity(city[1].idCity).subscribe(
      cities => {
        expect(cities).toBeTrue
        expect(cityServiceD.deleteCity).toBeTruthy()
      });
  });

  it('should update expected city(HttpClient called)', () => {
    const updateCity: ICity =
    {
      idCity: 1,
      cityName: 'Faca',
      amountPopulation: 59692,
      touristicPlace: 'Centro',
      reservedHotel: 'Principal'
    }

    httpClientSpyUpdate.put.and.returnValue(of(updateCity))

    cityServiceUpdate.updateCity(updateCity).subscribe(
      cities => {
        expect(cities).toEqual(updateCity)
        expect(cityServiceUpdate.updateCity).toBeTruthy();
      });
  });
});
