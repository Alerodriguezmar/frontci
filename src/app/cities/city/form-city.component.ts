import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { CityService } from './city.service'
import { ICity } from './icity';

@Component({
  selector: 'app-form-city',
  templateUrl: './form-city.component.html',
  styleUrls: ['./form-city.component.css']
})

export class FormCityComponent implements OnInit {
  cityForm = {} as ICity;
  titleForm = 'Registro Ciudad';

  constructor (private cityService: CityService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit (): void {
    this.cargar()
  }

  cargar (): void {
    this.activatedRoute.params.subscribe(
      e => {
        const idCity = e.idCity
        if (idCity) {
          this.cityService.getCity(idCity).subscribe(
            es => { this.cityForm = es }
          )
        }
      }
    )
  }

  create (): void {
    this.cityService.createCity(this.cityForm).subscribe(
      () => this.router.navigate(['/ciudad'])
    )
  }

  update (): void {
    this.cityService.updateCity(this.cityForm).subscribe(
     () => this.router.navigate(['/ciudad'])
    )
  }
}
