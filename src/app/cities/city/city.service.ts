import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from 'src/environments/environment'
import { ICity } from './icity';


@Injectable({
  providedIn: 'root'
})

export class CityService {
  private urlList: string = environment.baseUrl + '/ciudades';
  private url: string = environment.baseUrl + '/ciudad'

  constructor (private Http:HttpClient) { }

  /**
    * Obtiene todas la ciudades registradas.
    * @param urlList url a la cual se enviara la peticion get http para obtener todas las ciudades.
    * @returns retorna todas las ciudades registradas.
    */
  getAllCity (): Observable<ICity[]> {
    return this.Http.get<ICity[]>(this.urlList)
  }

  /**
   * Envia un objeto de tipo city para su creacion o insercion
   * @param url url a la cual enviara la peticion post http para la creacion del objeto.
   * @param city objeto de tipo ciudad el cual va a ser enviado
   */
  createCity (city: ICity): Observable<ICity> {
    return this.Http.post<ICity>(this.url, city)
  }

  /**
   * Obitiene un objeto unico de tipo city agregando su identificador a la url
   * @param idCity identificador de la ciudad a buscar o retornar.
   * @param url url a la cual se enviara la peticion http get y se le añadira el identificador
   * @returns una ciudad con un identificador de valor idCity.
   */
  getCity (idCity: number): Observable<ICity> {
    return this.Http.get<ICity>(this.url + '/' + idCity)
  }

  /**
   * Actualiza un objeto de tipo city ya existente.
   * @param url url a la cual se enviara la peticion put http para actualizar un objeto tipo city.
   * @param city Nuevos valores a tomar un objeto tipo city ya registrado.
   */
  updateCity (city: ICity): Observable<ICity> {
    return this.Http.put<ICity>(this.url, city)
  }

  /**
   * Elimina una ciudad ya existente por su identificador.
   * @param url url a la cual se enviara la peticion http delete  y se le añadira el identificador.
   * @param id identificador del objeto tipo city a eliminar.
   */
  deleteCity (id: number): Observable<ICity> {
    return this.Http.delete<ICity>(this.url + '/' + id)
  }
}
