export interface ICity {
  idCity:number;
  cityName: string;
  amountPopulation: number;
  touristicPlace: string ;
  reservedHotel: string ;
}
