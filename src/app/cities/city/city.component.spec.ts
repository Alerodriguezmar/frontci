import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CityComponent } from "./city.component"
import { CityService } from "./city.service";
import { of } from 'rxjs';
import { ICity } from "./icity";
import { RouterTestingModule } from "@angular/router/testing";

describe('city Component', () => {
  let cityHttpSpy: jasmine.SpyObj<CityService>;
  let fixture: ComponentFixture<CityComponent>
  let component: CityComponent;


  beforeEach((async () => {

    cityHttpSpy = jasmine.createSpyObj<CityService>('CityService', ['getAllCity', 'deleteCity','createCity']);

    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        { provide: CityService, useValue: cityHttpSpy }
      ],
      declarations: [CityComponent]
    }).compileComponents()
  }))
  
  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    CityComponent.prototype.ngOnInit = () => {} ;
    fixture = TestBed.createComponent(CityComponent);
    component = fixture.componentInstance
    fixture.detectChanges();
  })

  it('intance components', () => {
    expect(component).toBeDefined();
    expect(component).toBeInstanceOf(CityComponent);
    expect(component.cities.length).toBe(0);
  })

  it('add city', () => {
    component.cities = []
    const city:ICity = {
      idCity: 1,
      cityName: 'Funza',
      amountPopulation: 59692,
      touristicPlace: 'Centro',
      reservedHotel: 'Principal'
    } 

    expect(component.cities.length).toBe(0);
    cityHttpSpy.createCity.and.returnValue(of(city))
    component.create(city);
    expect(component.cities.length).toBe(1);
    expect(component.cities[0]).toBe(city)
  })
  it('Load Cities', () => {
    cityHttpSpy.getAllCity.and.returnValue(of([
      {
        idCity: 1,
        cityName: 'Funza',
        amountPopulation: 59692,
        touristicPlace: 'Centro',
        reservedHotel: 'Principal'
      } as ICity,
      {
        idCity: 2,
        cityName: 'Mosquera',
        amountPopulation: 423,
        touristicPlace: 'Centro',
        reservedHotel: 'Principal'
      } as ICity]));
    component.loadCity();
    expect(component.cities.length).toBe(2);
  })

  it('remove city', () => {
    component.cities = [{
      idCity: 1,
      cityName: 'Funza',
      amountPopulation: 59692,
      touristicPlace: 'Centro',
      reservedHotel: 'Principal'
    } as ICity]
    expect(component.cities.length).toBe(1);
    cityHttpSpy.deleteCity.and.returnValue(of(component.cities[0]))
    component.delete(component.cities[0])
    expect(component.cities.length).toBe(0);
  })

})
