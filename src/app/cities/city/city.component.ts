import { Component,Input,OnInit } from '@angular/core'
import { CityService } from './city.service'
import { ICity } from './icity';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html'
})
export class CityComponent implements OnInit {
  titulo = 'Ciudades';
  cities: ICity[] = [];


  constructor(private cityService: CityService) { }

  loadCity(): void {
    this.cityService.getAllCity().subscribe(
      city => { this.cities = city }
    )
  
  }

 

  delete(city: ICity): void {
    this.cityService.deleteCity(city.idCity).subscribe();
    this.cities = this.cities.filter(c => c.idCity != city.idCity);
  }

  create (city: ICity): void {
    this.cityService.createCity(city).subscribe(() => this.cities.push(city))
  }
  
  ngOnInit(): void {
    this.loadCity();
  }
}
