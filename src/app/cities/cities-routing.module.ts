import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CityComponent } from './city/city.component';
import { FormCityComponent } from './city/form-city.component';


const routes: Routes = [

  { path:  '',component:CityComponent},
  { path: 'form', component: FormCityComponent },
  { path: 'form/:idCity', component: FormCityComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CitiesRoutingModule { }
