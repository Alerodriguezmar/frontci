import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitiesRoutingModule } from './cities-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CityComponent } from './city/city.component';
import { FormCityComponent } from './city/form-city.component';


@NgModule({
  declarations: [
    CityComponent,
    FormCityComponent
  ],
  imports: [
    CommonModule,
    CitiesRoutingModule,
    HttpClientModule,
    FormsModule
  ]
})
export class CitiesModule { }
